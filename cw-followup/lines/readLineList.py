import csv
import numpy as np

def readLineList(filename):
    with open(filename) as lineList:

        readLineList = csv.reader(lineList,delimiter=',')
        lines = []
        for i,row in enumerate(readLineList): 
            """ 
            for each row, get line info
            i just keeps track of which line came from where for later
            """

            if row[1] == str('0'):
                """
                0: this is a single line. Read in central value, left width 
                   and right width
                """
                lines.append([float(row[0]),float(row[5]),float(row[6]),i])

            elif row[1]==str('1') or row[1]==str('2'):
                """
                1: this is a comb
                2: the is a comb with scaling width
                work out central values and left / right widths
                """
                freqOffset = float(row[2])
                freqSpacing = float(row[0])
                firstHarmonic = float(row[3])
                lastHarmonic = float(row[4])

                leftWidth = float(row[5])
                rightWidth = float(row[6])

                harmonics = np.arange(firstHarmonic,lastHarmonic+1,1)

                combList = [ freqOffset + (freqSpacing*float(h)) \
                                                    for h in harmonics ]

                """
                Now get width details, from the line list header: 
                % For fixed-width combs, veto the band:
                    [offset+index*spacing-leftwidth, 
                     offset+index*spacing+rightwidth]
                For scaling-width combs, veto the band:
                    [offset+index*spacing-index*leftwidth, 
                     offset+index*spacing+index*rightwidth]
                """
                if row[1]==str('1'):    
                    for c in combList: 
                        lines.append([c,leftWidth,rightWidth,i])
                elif row[1]==str('2'):
                    for c, h in zip(combList,harmonics):
                        modLeftWidth  = float(h) * leftWidth
                        modRightWidth = float(h) * rightWidth
                        lines.append([c,modLeftWidth,modRightWidth,i])


    return lines
