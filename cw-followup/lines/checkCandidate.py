import csv
from configparser import ConfigParser
from numpy import pi
import readLineList 
import lineResultsPlot



def lineLookUp(indexList,lineFile): 
    """ 
    get the original details for the line
    """
    saveLines = [None] * len(indexList)
    count=0
    with open(lineFile) as lineList:
        originalLineList = csv.reader(lineList,delimiter=',')
        for i,row in enumerate(originalLineList):
            if i in indexList: 
                saveLines[count]=row 
                count+=1
    return saveLines



def getBinaryFactor(p,a,fline):
    """
    compute the binary factor 
    """     

    return (2.*pi*a*fline)/p


def checkInRange(f,lines,binary=False,period=None,asini=None): 
    """ 
    See if the candidate frequency overlaps with a noise line
    """
    vetoedBy=[]

    for l in lines:
        centre,left,right = l[0],l[1],l[2]

        if binary==True: binaryFactor = getBinaryFactor(period,asini,centre)
        else: binaryFactor = 0.0

        if f<(centre+right+binaryFactor) and f>(centre-left-binaryFactor):
            vetoedBy.append(l[3]) # can be vetoed by multiple lines
        else: pass

    return vetoedBy
    




def doLineVeto(lineFile,frequency,ifo,binary=False,period=None,asini=None):

    lines = readLineList.readLineList(lineFile)
    lineIDs = checkInRange(frequency,lines)
    if len(lineIDs)==0:
        print('Not vetoed by {}1'.format(ifo))
    else: 
        lineDetails = lineLookUp(lineIDs,lineFile)
        print('vetoed by {} {}1 line(s):'.format(len(lineDetails),ifo))
        print(lineDetails)
    # make the plot
    lineResultsPlot.plotLines(frequency,lines,whichDetector=ifo,binary=binary,period=period,asini=asini)

    return None




def main():

    # read in ini file values
    config = ConfigParser()
    config.read('test.ini')

    ifos = list(config.get('data','ifo'))
    
    # git repo at https://git.ligo.org/CW/instrumental/aLIGO-lines-combs
    # including local path for now:
    pathToLineFiles = '/home/hannahm/repositories/aLIGO-lines-combs'


    obsRun = int(config.get('data','obsrun')) # this option does not work yet
    print(obsRun,type(obsRun))
    if obsRun == 2:
        H1LineFile = pathToLineFiles+'/O2/O2H1lines.csv'
        L1LineFile = pathToLineFiles+'/O2/O2L1lines.csv'
    elif obsRun == 3:
        H1LineFile = pathToLineFiles+'/O3/O3H1lines.csv'
        L1LineFile = pathToLineFiles+'/O3/O3L1lines.csv'
    else:   
        print('Error: choose 2 or 3 for observing run')
        exit()


    frequency = float(config.get('candidate', 'frequency'))


    isBinary = str(config.get('candidate','isbinary')) # this option does not work yet
    if isBinary != 'True' and isBinary != 'False':
        print('Error: isBinary should be True or False')
        exit()


    if isBinary=='False':
       
        if 'H' in ifos:
            doLineVeto(H1LineFile,frequency,'H')
        if 'L' in ifos:
            doLineVeto(L1LineFile,frequency,'L')

    
    elif isBinary=='True':

        # get binary parameters
        period = float(config.get('candidate','period'))

        asini = float(config.get('candidate','asini'))
     

        if 'H' in ifos:
            doLineVeto(H1LineFile,frequency,'H',\
                       binary=True,period=period,asini=asini)
        if 'L' in ifos:
            doLineVeto(L1LineFile,frequency,'L',\
                       binary=True,period=period,asini=asini)


if __name__ == "__main__":
    main()
