import numpy as np
import matplotlib.pyplot as plt
import readLineList
import checkCandidate


def doFilling(lineList,col,lo):

    for row in lineList:
        centre,left,right = row[0],row[1],row[2]
        if centre-left>lowF and centre+right<highF: 
            ax1.fill_between((centre-left,centre+right),(-1,-1),(1,1),\
                             color=col,alpha=0.7)
            ax1.plot(([centre,centre]),([-1,1]),color=col)
    return


def plotLines(frequency,lineList,whichDetector='',binary=False,period=None,asini=None):

    if whichDetector=='H': col = 'r'
    elif whichDetector=='L': col = 'b'

    y = np.zeros(10)    
    x = np.random.rand(10)

    
    #plt.scatter(0.0,0,'x')
    lowF,highF = frequency-20, frequency+20


    fig = plt.figure()
    ax1 = fig.add_subplot(111) 
    ax1.set_aspect(0.2)


    # plotting the binary expansion underneath the line widths
    if binary==True: 

        for row in lineList:

            centre,left,right = row[0],row[1],row[2]
            binaryFactor = checkCandidate.getBinaryFactor(period,asini,centre)
            leftMod  = left+binaryFactor
            rightMod = right+binaryFactor
            if centre-leftMod>lowF and centre+rightMod<highF:
                ax1.fill_between((centre-leftMod,centre+rightMod),(-1,-1),(1,1),\
                                  color=col,alpha=0.3)
    
    test=[]
    for row in lineList:
        centre,left,right = row[0],row[1],row[2]
        test.append(left)
        test.append(right)
        if centre-left>lowF and centre+right<highF: 
            ax1.fill_between((centre-left,centre+right),(-1,-1),(1,1),\
                             color=col,alpha=0.7)
            ax1.plot(([centre,centre]),([-1,1]),color=col)


    if whichDetector=='H': detectorLabel='LIGO-Hanford'
    elif whichDetector=='L': detectorLabel='LIGO-Livingston'

    ax1.set_xlim(frequency-2,frequency+2)
    ax1.set_ylim(-1,1)
    ax1.scatter(frequency,0,marker='x',s=200,color='k',label='candidate')
    ax1.set_xlabel("Frequency (Hz)")
    ax1.set_yticks([])
    plt.title('{}'.format(detectorLabel))
    plt.tight_layout()
    plt.legend(loc=2)
    plt.savefig('{}.png'.format(detectorLabel))
    plt.show()


    return None    


