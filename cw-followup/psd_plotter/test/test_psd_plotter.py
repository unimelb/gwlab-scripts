from psd_plotter.psd_plotter import make_sft_list_file

def test_o1_there_are_lines_in_the_file():
    candidate = o1_candidate()
    filename = make_sft_list_file(candidate)
    with open(filename, 'r') as fh:
        num_lines = len(fh.readlines())
    assert num_lines > 10

def test_o2_filename_is_equal_to_the_thing_to_which_it_should_be_equal():
    candidate = o2_candidate()
    filename = make_sft_list_file(candidate)
    assert filename == "sfts_o2_1164562334_1187733592.list"

def test_o2_there_are_lines_in_the_file():
    candidate = o2_candidate()
    filename = make_sft_list_file(candidate)
    with open(filename, 'r') as fh:
        num_lines = len(fh.readlines())
    assert num_lines > 10

def o1_candidate():
    return {
        'source_dataset': 'o1',
        'candidate_frequency': 188.0,
        'target_binary': True,
        'orbit_tp': 1238161512.786,
        'asini': 0.01844,
        'orbit_period': 4995.263,
    }

def o2_candidate():
    return {
        'source_dataset': 'o2',
        'candidate_frequency': 188.0,
        'target_binary': True,
        'orbit_tp': 1238161512.786,
        'asini': 0.01844,
        'orbit_period': 4995.263,
    }
