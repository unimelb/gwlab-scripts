#!/usr/bin/env python

"""'PSD Plotter' followup script for CWFollowup module

# Overview

The science aim of this script is to produce plot(s) that give the reader an
idea of what the noise spectrum looks like near their proposed candidate. For
each candidate (specified in the input JSON), it produces a single output plot.
The script expects its main input to be a standard-format JSON file. There are
some flags that control precisely what it produces, but for now we should
assume the defaults of these flags are correct.


# Implementation notes

The script leans heavily on lalapps_ComputePSD to do the underlying technical
task of computing the power spectral density (PSD). l_CPSD has a large number
of options to control precisely how SFTs are defined. For now we use the
defaults for these options (which match how the published plots are produced,
in most cases) but some thought should be given to whether this is appropriate.

The script proceeds as follows:
 * identify the size of the frequency space to be plotted (based on binary comb
   size)
 * identify the time range used for the candidate itself and thus the SFTs that
   need to be used
 * ask lCPSD to produce the data for the plot

If multiple candidates are specified, multiple plots will be created.

Once data are produced for the plot, the script will invoke gnuplot to create
physical PNG files.


# Input format

The script expects a --json_file argument to a JSON file containing information
about the candidate and original source information.

TODO: include format spec here


# Testing

There is an example JSON file in tests/. Building this into a formal test suite
is a task for the future, but in the meantime:
  python psd_plotter.py test/00_example.json
... but be aware it expects to have access to the O2 SFTs, so best to do this
on Ozstar or it simply won't work

"""

import sys
import json
import argparse
import glob
import os.path
import subprocess
import io
from math import pi, sqrt

def main():
    """Parse arguments, loop over candidates, and plot things"""

    args = parse_arguments()
    try:
        with open(args.json_file[0], 'r') as fh:
            input = json.load(fh)
    except OSError as e:
        print(f"Error opening '{args.json_file[0]}' for reading")
        print(e)
        return 1

    if not 'candidates' in input or not len(input['candidates']):
        print("""No candidates specified in JSON.
This is not strictly speaking an error but it is a bit anomalous...""")
        return 0

    for c in input['candidates']:
        plot_psd_for_candidate(c, input)

    return 0

def plot_psd_for_candidate(c, input):
    
    # Make a list of SFTS
    sft_list_file = make_sft_list_file(c)
    print(f"SFTlist: {sft_list_file}")

    # Name the output files
    # TODO: Would it make sense to use some kind of index, rather than this
    # horrifying combination of parameters, in the output file name?
    if c['target_binary']:
        output_stem = f"PSD_{c['candidate_frequency']:.10f}_{c['orbit_period']:.6f}_{c['asini']:.6f}_{c['orbit_tp']:.3f}";
    else:
        output_stem = f"PSD_{c['candidate_frequency']:.10f}_iso";

    # Run lalapps_ComputePSD
    invoke_lCPSD(c, sft_list_file, output_stem + '.tsv')

    # Do the plot
    gnuplot_script = generate_gnuplot(output_stem + '.tsv', output_stem + '.png', c)
    invoke_gnuplot(gnuplot_script)

def invoke_lCPSD(c, sft_list_file, outname):
    """Call lalapps_ComputePSD and return the resulting PSD

    Expects the candidate, list of SFTs and output name as arguments.
    """

    # We need to put together these args:
    #  * --inputData=sft_list_file
    #  * --outputPSD=output file name
    #  * --Freq     start frequency
    #  * --FreqBand width of the band
    # ... which we compute in roughly that order.
    #
    # For future expansion, it might also make sense to support binSizeHz and
    # some of the different methods of combining PSDs (e.g. PSDmthop{SFT,IFO}s)

    # We show a region in frequency space around the candidate. How big a
    # region? It should be very small (because the PSD is bumpy), but we need
    # to cover the region covered by the J-stat "wings"; furthermore, we want
    # to add a bit more to make the plot look good.

    if c["target_binary"]:
        wing_width = getBinaryFactor(c['orbit_period'], c['asini'], c['candidate_frequency'])
    else:
        wing_width = 0.

    # add 5% on both sides
    plot_width = wing_width * 0.05
    # if the plot width is small, expand it to display a reasonable number of bins
    # this case also catches isolated candidates
    # TODO: depend on Tsft, when that's available in the input
    if plot_width * 1800. < 120:
        plot_width = 120./1800.

    # *2 to cover both sides
    # TODO: Should we add a timeout here to avoid hanging?
    print(f"freq: {wing_width}; band: ;");
    try:
        lcpsd_result = subprocess.run(['lalapps_ComputePSD',
            '--inputData', 'list:' + sft_list_file,
            '--outputPSD', outname,
            '--Freq', f"{c['candidate_frequency']-plot_width:.10f}",
            '--FreqBand', f"{2*plot_width:.10f}",
            ],
            check=True,
        );
    except subprocess.SubprocessError as e:
        print("Failed to run lalapps_CPSD.")
        raise

def invoke_gnuplot(script):
    """Call gnuplot and pass it the script via standard input."""

    try:
        gnuplot_result = subprocess.run(['gnuplot'],
            check=True,
            input=script,
            encoding='UTF-8',
        );
    except subprocess.SubprocessError as e:
        print("Failed to run gnuplot.")
        raise

def generate_gnuplot(infilename, outfilename, c):
    # Rather annoyingly there's no way to ask gnuplot to put an arrow in that's
    # most but not all of the y-range, so we need to estimate the axis
    (arrow_hi, arrow_lo) = estimate_y_range(infilename)
    arrow_lo = sqrt(arrow_lo)
    arrow_hi = sqrt(arrow_hi)

    return_val = f"""
set term pngcairo dashed
set output '{outfilename}'

set ylabel "One-sided PSD [Hz^1/2]"
set xlabel "Frequency [Hz]"
set format y '%.1e'

set arrow from {c['candidate_frequency']}, {arrow_lo} to {c['candidate_frequency']}, {arrow_hi} nohead front lw 2 lt 7
"""
    if c['target_binary']:
        wing_width = getBinaryFactor(c['orbit_period'], c['asini'], c['candidate_frequency'])
        low_wing = c['candidate_frequency'] - wing_width/2
        high_wing = c['candidate_frequency'] + wing_width/2
        return_val += f"""
set arrow from {low_wing}, {arrow_lo} to {low_wing}, {arrow_hi} nohead front lt 9
set arrow from {high_wing}, {arrow_lo} to {high_wing}, {arrow_hi} nohead front lt 9
"""

    return_val += f"""
plot '{infilename}' using 1:(sqrt($2)) with boxes title "", \
  1/0 lw 2 lt 7 title "Candidate central frequency" """
    if c['target_binary']:
        return_val += f""", \
  1/0 lt 9 title "Binary orbit Dopper modulation"

"""

    return return_val

def estimate_y_range(infilename):
    max = 0.
    min = 1.
    with open(infilename, 'r') as fh:
        for l in fh.readlines():
            if len(l) == 0 or l[0] == '%':
                continue
            (_, v) = l.split()
            fv = float(v)
            if fv > max:
                max = fv
            elif fv < min:
                min = fv
    return (max, min)

# TODO: separate this out into a library of its own
def getBinaryFactor(p,a,fline):
    """
    compute the binary factor 
    """     

    return (2.*pi*a*fline)/p

def make_sft_list_file(c):
    """Make a list of SFTs used by a candidate and then write them to a file

    The format of the file is as lalapps_ComputePSD expects to read them. The
    following algorithm is how the SFTs are selected:
     * is the Viterbi original start/end time information available? if so, the
     SFTs captured by that (NOT YET IMPLEMENTED)
     * otherwise - the full time period for the source_dataset

    Returns the name of the file
    """

    # For now we hardcode these OzStar locations. Note also the start and end
    # times refer to the SFT datasets; be careful before re-using them as 'the'
    # start and end times of O2/O3...
    datasets = {
        'o1': { 'name': 'O1', 'start': 1126623617, 'end': 1137252079, 'glob':
        '/datasets/LIGO/private/O1/pulsar/sfts/tukeywin/*_C01/*/*.sft' },
        'o2': { 'name': 'O2', 'start': 1164562334, 'end': 1187733592, 'glob':
        '/datasets/LIGO/private/O2/pulsar/sfts/tukeywin/*_C02/*/*.sft' },
        #'/home/patrick/swinburne/gwlab/cwfollowup/gwlab-scripts/cw-followup/psd_plotter/sfts/*.sft' },
        'o3': { 'name': 'O3', 'start': 1238166049, 'end': 1260003571, 'glob':
        '/datasets/LIGO/private/O3/pulsar/sfts/tukeywin/?1_C01_Gated_1800s/*/*.sft' },
    }

    # at some point we'll support more complex filtering logic
    ds = datasets[c["source_dataset"]]
    assert ds # die if it's a dataset we don't have
    start = ds['start']
    end = ds['end']

    # Name the file - this is a function of the detectors used, start and end
    # times. We can then check if the file exists and, if so, re-use it (which
    # is the happy path for multi-candidate IFOs)
    filename = f"sfts_{c['source_dataset']}_{start}_{end}.list";
    if os.path.exists(filename):
        return filename

    allowed_files = []
    filter_predicate = sft_filter_func(start, end, set(['H', 'L']))
    for d in datasets.values():
        if start >= d['start'] and start < d['end']:
            allowed_files.extend(filter(filter_predicate, glob.glob(d['glob'])))

    with open(filename, 'w') as fh:
        for f in allowed_files:
            fh.write(f"{f}\n")
    return filename

def sft_filter_func(start, end, allowed_dets):
    """Return a predicate determining if a given SFT file*name* is valid

    start and end are the start and end times in the usual way

    allowed_dets should be a set of allowed detectors. At the moment it looks
    like we have no way of getting these from the JSON, so just pass H and L
    """
    def F(name):
        # SFT files must match this form:
        #  H-1_H1_1800SFT_O2-1164909552-1800.sft
        # i.e. dash separated into detector, comment, start time, length
        # see LIGO-T040164-01-Z
        (dtor, _, start_time, tsft) = os.path.basename(name).split("-")
        assert(tsft=="1800.sft")
        if dtor not in allowed_dets:
            return False
        start_time_int = int(start_time)
        # just check whether the start of the SFT is in the interval
        # this follows the approach in CFS and friends
        if start_time_int >= start and start_time_int <= end:
            return True
        return False

    return F


def parse_arguments():
    parser = argparse.ArgumentParser(
      description="'PSD Plotter' followup script for CWFollowup module")
    parser.add_argument('json_file', nargs=1,
      help='Input JSON file (in standard format)')

    return parser.parse_args()

if __name__ == "__main__":
    sys.exit(main())
