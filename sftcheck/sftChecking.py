import os
import glob
import numpy as np

"""
Checks whether SFTs between two times are present on OzSTAR
Includes O1, O2, O3
To do: check which calibration is used for each data set. 
"""



def getStartTimes(HFiles, LFiles):
    HTimes = [ int(filename[-19:-9]) for filename in HFiles ]
    LTimes = [ int(filename[-19:-9]) for filename in LFiles ]
    return HTimes, LTimes

# not used
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def findNSFTs(array, startValue, endValue):
    """
    How many SFTs are there between the start time and end time?
    """
    array = np.asarray(array)
    idxs = np.where((array<=endValue) & (array>=startValue))[0]
    if len(idxs)==0:
        return 0
    else:
        number = max(idxs) - min(idxs)
        return number

# getting start times from OzSTAR. 
# need to check which calib we are using

# O1
O1Cal = '_C01'
O1LHOList = glob.glob('/datasets/LIGO/private/O1/pulsar/sfts/tukeywin/LHO{}/*/*-1800.sft'.format(O1Cal))
O1LLOList = glob.glob('/datasets/LIGO/private/O1/pulsar/sfts/tukeywin/LLO{}/*/*-1800.sft'.format(O1Cal))
O1LHO, O1LLO = getStartTimes(O1LHOList, O1LLOList)

# O2 
O2Cal = '_C02'
O2LHOList = glob.glob('/datasets/LIGO/private/O2/pulsar/sfts/tukeywin/LHO{}/*/*-1800.sft'.format(O2Cal))
O2LLOList = glob.glob('/datasets/LIGO/private/O2/pulsar/sfts/tukeywin/LLO{}/*/*-1800.sft'.format(O2Cal))
O2LHO, O2LLO = getStartTimes(O2LHOList, O2LLOList)

# O3
O3Cal = '_C01_Gated_Sub60Hz_1800s'
O3LHOList = glob.glob('/datasets/LIGO/private/O3/pulsar/sfts/tukeywin/H1{}/*/*-1800.sft'.format(O3Cal))
O3LLOList = glob.glob('/datasets/LIGO/private/O3/pulsar/sfts/tukeywin/L1{}/*/*-1800.sft'.format(O3Cal))
O3LHO, O3LLO = getStartTimes(O3LHOList, O3LLOList)


# all start times 
allLHO = sorted(O1LHO + O2LHO + O3LHO)
allLLO = sorted(O1LLO + O2LLO + O3LLO)

print(type(allLHO))
print(type(allLHO[789]))
print(allLHO[789])

# parameters from the GWLab form
searchTdrift = 864000 # 10 days
nBlocks = 170
searchStartTime = 1126051217 
searchEndTime   = searchStartTime + searchTdrift*nBlocks

expectedNoSFTsPerBlock = searchTdrift / 1800

# how many sfts are there between each time block? empty arrays for plot
dutyCycleLHO = np.zeros(nBlocks)
dutyCycleLLO = np.zeros(nBlocks)
for i in range(nBlocks):

  start = searchStartTime + (searchTdrift*i)
  end   = start+searchTdrift

  # finds the number of SFTs in this time range
  nSFTsLHO = findNSFTs(allLHO,start,end)
  nSFTsLLO = findNSFTs(allLLO,start,end)

  dutyCycleLHO[i] = nSFTsLHO/expectedNoSFTsPerBlock
  dutyCycleLLO[i] = nSFTsLLO/expectedNoSFTsPerBlock



  print('block {} LHO {:.2f}% LLO {:.2f}% '.format(i,100.*dutyCycleLHO[i],100.*dutyCycleLLO[i]))



import matplotlib.pyplot as plt
x = np.arange(0,nBlocks,1)

fig, axs = plt.subplots(2)
axs[0].bar(x,dutyCycleLHO,color='r')
axs[0].set_ylabel('LHO fraction')
axs[0].set_ylim(0,1)
axs[1].bar(x,dutyCycleLLO,color='b')
axs[1].set_ylabel('LLO fraction')
axs[1].set_xlabel('block number')
axs[1].set_ylim(0,1)
#plt.savefig('test_O1O2O3.png')
plt.show()



 
