"""
Draw random positions on the sky for I'm feeling lucky search.
"""

import numpy as np


def drawRAAndDec():

    """
    draw position in RA and Dec
    """

    # ra - draw randomly from 0 to 2pi
    ra = np.random.uniform(0,2.*np.pi)
    

    # dec - draw randomly from -1 to 1 in sin(dec)
    sinDec = np.random.uniform(-1,1)    
    # and convert from sin(dec) to dec
    dec = np.arcsin(sinDec)

    
    return ra, dec






# testing with with plot
nDraws = 10000
ras  = np.zeros(nDraws)
decs = np.zeros(nDraws)
for i in range(nDraws):

    ras[i],decs[i] = drawRAAndDec()

print(ras)
print(decs)

import matplotlib.pyplot as plt
plt.scatter((180.*ras)/np.pi,(180.*decs)/np.pi)
plt.show()

